# MONOREPOC

### PREREQUISITES

- **NodeJS**: *18.12.1*
- **Yarn**: *1.22.19*
- Or **NPM**: *8.19.2*

### STACK

lerna, typescript v5.0.2, CI/CD (*gitlab-ci*), eslint, prettier [...]

#### Client

nextjs: v13.2.4, reactjs: v18.2.0, @apollo/client, @material-ui, cypress v12.8.1 [...]

#### Server

graphql, nodejs (expressjs), nodemon, jest, jsonwebtoken (jwt) [...]

### SPECs

- [x] Create a mono repo (front + back app)
- [x] Use npm or yarn (as you want)

**User stories**:

- [x] Register (firstname, lastname, email, password)
- [x] Login (email, password)
- [x] Logout
- [x] Welcome page with user email after login

**Backend spec**:

- [x] Use NodeJS (expressjs), Typescript
- [x] Add GraphQL API (with my lib/architecture)
- [x] Jest for tests (all endpoints should be covered)
- [x] For easier implementation you can use https://www.npmjs.com/package/lru-cache to simulate an in memory database
- [x] Implement a `me` or `viewer` query with a session token that return the current logged user
- [x] Use a JWT system to deliver the session token after login/register
- [x] No need to hash password or add some security features, here we don't care about these things, it's just a POC
- [x] `npm test` or `yarn test` should run all tests and report coverage (--coverage on jest CLI)
- [x] I can suggest to use https://www.npmjs.com/package/ts-jest plugin for jest

**Front spec**:

- [x] Use NextJS (Typescript), Apollo client, MaterialUI
- [x] Add cypress tests to cover User stories (Use https://www.npmjs.com/package/start-server-and-test to start the backend during front testing).
- [x] Use localStorage to store the JWT
- [x] `npm test` or `yarn test` should start the backend and run all FRONT Cypress tests (coverage not needed)

**DevOps spec**:

- [x] Write a gitlab ci workflow which run both front and back tests on the CI.
- [x] Add README instructions to install and test the POC

### Installation
```
yarn
```

### Start project (client + server)
```
yarn start
```

*Wait until you see this before*
...
`[ser] Server listen on PORT 4000`
`[cli] event - compiled successfully`

*Then open*
> Url client: http://localhost:3000


### Start client (separately)
```
yarn client
```

### Start server (separately)
```
yarn server
```

> Url server: http://localhost:4000
> GraphQL playground: http://localhost:4000/graphql

### Client tests

*NB:* The tests run everything they need to run automatically. No need to have another server running.

##### OPTION 1 (From root project)
```
yarn client:test
```
##### OPTION 2 (From client project)
```
cd app/client
yarn test
```

### Server tests (with coverage)

*NB:* The tests run everything they need to run automatically. No need to have another server running.

##### OPTION 1 (From root project) - recommanded
```
yarn server:test
```
##### OPTION 2 (From server project)
```
cd app/server
yarn test
```

### Start lint
```
yarn lint
```

### Start lint fix
```
yarn lint:fix
```