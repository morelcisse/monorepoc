import { useState } from "react";
import { gql } from "@apollo/client";
import { apolloClient } from "common/helpers/Apollo";
import { commonStyles } from "src/styles/main";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import { clearSession, connected, token, username } from "src/helpers";
import { Button } from "@material-ui/core";
import { useRouter } from "next/router";
import { useSnackbar } from "notistack";
import LinearProgress from "@material-ui/core/LinearProgress";

const Navbar = () => {
	const { enqueueSnackbar } = useSnackbar();
	const [submited, setSubmited] = useState(false);
	const router = useRouter();
	const classes = commonStyles();
	const logout = () => {
		setSubmited(true);
		apolloClient
			.query({
				query: gql`
					query ($access_token: String!) {
						logout(access_token: $access_token) {
							... on MsgType {
								message
								error
							}
						}
					}
				`,
				variables: { access_token: token() },
			})
			.then(({ data }) => {
				const result = data.logout;

				enqueueSnackbar(result.message, { variant: "success", autoHideDuration: 1000 });
				setTimeout(() => {
					router.push("index", "/");
					clearSession();
				}, 500);
			});
	};

	return (
		<div className={classes.root}>
			<AppBar position="static">
				<Toolbar>
					<Typography variant="h6" className={`nav-title ${classes.navTitle}`}>
						{username()}
					</Typography>

					{connected() && (
						<Button onClick={logout} id="logout" color="inherit">
							Déconnexion
						</Button>
					)}
				</Toolbar>
			</AppBar>
			{submited && <LinearProgress color="secondary" />}
		</div>
	);
};

export default Navbar;
