import { FC, PropsWithChildren } from "react";
import { Grid, GridSize } from "@material-ui/core";
import Typography from "@material-ui/core/Typography";
import { commonStyles } from "src/styles/main";

const GridLayout: FC<
	PropsWithChildren<any> & { title?: string; xs?: boolean | GridSize; minHeight?: string }
> = ({ children, title, xs, minHeight = "100vh" }) => {
	const classes = commonStyles();

	return (
		<Grid
			container
			spacing={0}
			direction="row"
			alignItems="center"
			justifyContent="center"
			style={{ minHeight, backgroundColor: "#eceff1" }}
		>
			<Grid item xs={xs || 4}>
				{title && (
					<Typography variant="h5" className={classes.title} gutterBottom>
						{title}
					</Typography>
				)}

				{children}
			</Grid>
		</Grid>
	);
};

export default GridLayout;
