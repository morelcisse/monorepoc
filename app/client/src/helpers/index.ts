const username = () => {
	const name = window.localStorage.getItem("username");

	return name;
};

const token = () => {
	const token = window.localStorage.getItem("token");

	return token;
};

const connected = () => {
	return token() !== null;
};

const clearSession = () => {
	window.localStorage.removeItem("token");
	window.localStorage.removeItem("username");
};

export { connected, token, username, clearSession };
