import Button from "@material-ui/core/Button";
import { commonStyles } from "src/styles/main";
import Link from "next/link";
import CenteredLayout from "src/components/Layout/GridLayout";
import Box from "@mui/material/Box";

const Homepage = () => {
	const classes = commonStyles();

	return (
		<CenteredLayout title="INTERVIEW">
			<Box textAlign="center">
				<Link className={classes.linkNone} href="register" as="/register" passHref>
					<Button
						size="large"
						variant="contained"
						color="primary"
						id="register"
						className={classes.button}
					>
						Inscrivez-vous
					</Button>
				</Link>
				-
				<Link className={classes.linkNone} href="login" as="/login" passHref>
					<Button
						size="large"
						variant="contained"
						color="secondary"
						id="login"
						className={classes.button}
					>
						Connectez-vous
					</Button>
				</Link>
			</Box>
		</CenteredLayout>
	);
};

export default Homepage;
