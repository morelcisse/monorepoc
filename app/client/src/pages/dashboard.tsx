import UserInfos from "src/components/UserInfos/UserInfos";
import dynamic from "next/dynamic";
import { Box, Container, Grid } from "@material-ui/core";
import { commonStyles } from "src/styles/main";
import { useEffect } from "react";
import { useRouter } from "next/router";
import { connected } from "src/helpers";

const NavbarWithNoSSR = dynamic(() => import("src/components/AppBar/AppBar"), { ssr: false });

const Dashboard = ({}) => {
	const classes = commonStyles();
	const router = useRouter();

	useEffect(() => {
		if (!connected()) {
			router.push("index", "/");
		}
	}, []);

	return (
		<Container maxWidth="xl" className={classes.dashContainer}>
			<NavbarWithNoSSR />

			<Grid container spacing={0} direction="row" alignItems="center" justify="center">
				<Grid item xs={6}>
					<Box component="div" mt={12}>
						<UserInfos />
					</Box>
				</Grid>
			</Grid>
		</Container>
	);
};

export default Dashboard;
