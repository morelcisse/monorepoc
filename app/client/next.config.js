const path = require("path");
const { PHASE_DEVELOPMENT_SERVER } = require("next/constants");

const dev = process.env.NODE_ENV === "development";

const Config = (phase, { defaultConfig }) => {
	let baseConfig = {
		...defaultConfig,
		compress: !dev,
		publicRuntimeConfig: {
			app_env: process.env.NODE_ENV,
		},
		trailingSlash: false,
	};

	if (phase === PHASE_DEVELOPMENT_SERVER) {
		baseConfig = {
			...baseConfig,
			webpack(webpackConfig) {
				webpackConfig.resolve.alias = webpackConfig.resolve.alias || {};
				webpackConfig.resolve.alias["src"] = path.resolve(__dirname, "src");

				return webpackConfig;
			},
		};
	}

	return { ...baseConfig };
};

module.exports = Config;
