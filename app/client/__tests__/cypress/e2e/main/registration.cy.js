/// <reference types="cypress" />

context("Registration", () => {
	const url = "http://localhost:3000";

	beforeEach(() => {
		cy.visit(url);
	});

	it("The user registers", () => {
		cy.fixture("users.json").then((u) => {
			const user = u[0];

			cy.get("#register").click();

			cy.wait(100); // for cypress open, you can remove or comment as you want

			cy.url().should("eq", `${url}/register`);

			cy.get("#register-submit").click();

			cy.get("#register-firstname-helper-text").contains("Ce champs es requis");
			cy.get("#register-lastname-helper-text").contains("Ce champs es requis");
			cy.get("#register-email-helper-text").contains("Ce champs es requis");
			cy.get("#register-password-helper-text").contains("Ce champs es requis");

			cy.wait(600); // for cypress open, you can remove or comment as you want

			cy.get("input[name='firstname']").type(user.firstname);
			cy.get("input[name='lastname']").type(user.lastname);
			cy.get("input[name='email']").type(user.email);
			cy.get("input[name='password']").type(user.password);

			cy.get("#register-firstname-helper-text").should("not.exist");
			cy.get("#register-lastname-helper-text").should("not.exist");
			cy.get("#register-email-helper-text").should("not.exist");
			cy.get("#register-password-helper-text").should("not.exist");

			cy.get("#register-submit").click();

			cy.get("#notistack-snackbar").as("alert");
			cy.get("@alert")
				.invoke("text")
				.then((text) => {
					if (text !== "Cet utilisateur existe déjà") {
						cy.wait(600);
						cy.url().should("eq", `${url}/login`);
					} else {
						expect(text).to.satisfy((responseText) =>
							[
								"Votre compte à été crée. Vous pouvez vous connectez",
								"Cet utilisateur existe déjà",
							].includes(responseText)
						);
						cy.wait(100); // for cypress open, you can remove or comment as you want
						cy.url().should("eq", `${url}/register`);
					}
				});
		});
	});
});
