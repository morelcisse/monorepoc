import cachedb from "src/extras/db";
import { decodedToken, generateAccessToken } from "src/extras/helpers";
import { IUser } from "common/interfaces/User";

const Resolvers = {
	/** Get user */
	me: ({ access_token }) => {
		const email = decodedToken(access_token).email;

		if (cachedb.has(email) && cachedb.has(access_token)) {
			const user: IUser = cachedb.get(email) as IUser;

			return {
				__typename: "UserType",
				...user,
				access_token,
			};
		}

		return {
			__typename: "MsgType",
			message: "Cet utilisateur n'existe pas",
			error: true,
		};
	},
	/** Register user */
	register: ({ user }) => {
		const { email } = user as IUser;

		if (!cachedb.has(email)) {
			cachedb.set(email, { ...user, connected: true });
			return {
				__typename: "MsgType",
				message: "Votre compte à été crée. Vous pouvez vous connectez",
				error: false,
			};
		}

		return {
			__typename: "MsgType",
			message: "Cet utilisateur existe déjà",
			error: true,
		};
	},
	/** Login user */
	login: ({ user }) => {
		const { email, password } = user as IUser;

		if (cachedb.has(email)) {
			let user: IUser = cachedb.get(email) as IUser;
			const access_token = generateAccessToken(email);
			user = { ...user, connected: true };

			if (user.password === password) {
				cachedb.set(email, user);
				cachedb.set(access_token, email);
				return {
					...user,
					__typename: "UserType",
					access_token,
				};
			}
			return {
				__typename: "MsgType",
				message: "Vos identifiants de connexion sont incorrectes",
				error: true,
			};
		}

		return {
			__typename: "MsgType",
			message: "Cet utilisateur n'existe pas",
			error: true,
		};
	},
	/** Logout user */
	logout: ({ access_token }) => {
		const email = decodedToken(access_token).email;
		const user: IUser = cachedb.get(email) as IUser;

		cachedb.delete(access_token);
		cachedb.set(email, { ...user, connected: false });
		return {
			__typename: "MsgType",
			message: "Vous êtes déconnecté",
			error: false,
		};
	},
	/** Delete user (Used at end of all tests) */
	deleteUser: ({ access_token }) => {
		const email = decodedToken(access_token).email;
		const hasEmail = cachedb.has(email);

		if (hasEmail) {
			cachedb.delete(email);
			cachedb.delete(access_token);
		}

		return {
			__typename: "MsgType",
			message: hasEmail ? "L'utilsateur à été supprimé" : "Cet utilisateur n'existe pas",
			error: !hasEmail,
		};
	},
};

export default Resolvers;
