import LRU from "lru-cache";

const options = {
	max: 500,
	maxAge: 86400000,
};

const cachedb = new LRU(options);

export default cachedb;
