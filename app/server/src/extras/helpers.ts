import jwt from "jsonwebtoken";

const generateAccessToken = (email: string, secret?: string): string => {
	const access_token = jwt.sign({ email }, process.env.JWT_TOKEN_SECRET || secret, {
		expiresIn: "1h",
	});

	return access_token;
};

const decodedToken = (access_token: string) => {
	const decoded = jwt.decode(access_token, { json: true, completed: true });

	return decoded;
};

export { generateAccessToken, decodedToken };
