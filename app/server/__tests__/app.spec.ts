import axios from "axios";

import { generateAccessToken, decodedToken } from "../src/extras/helpers";

describe("App tests", () => {
	const serverURL = "http://localhost:4000/";
	const email = "morelcisse@test.mail";
	const access_token = generateAccessToken(email, "2528a64b-d9b4-49f6-9b44-ea088bb29157");

	it("Server's up !", async () => {
		const { data, status } = await axios.get(serverURL);
		const message = data;

		expect(message).toEqual("Run Cypress tests !");
		expect(status).toBe(200);
	});

	it("Generate access token", () => {
		expect(typeof access_token).toBe("string");
		expect(access_token.length).toBeGreaterThan(0);
	});

	it("Decode token", () => {
		const decoded = decodedToken(access_token);

		expect(decoded).toHaveProperty("email", email);
		expect(decoded.email).toEqual(email);
	});
});
