import { gql } from "@apollo/client";
import { apolloClient } from "../../common/helpers/Apollo";
import { IUser } from "../../common/interfaces/User";

describe("Main tests", () => {
	let testUser: IUser = {
		firstname: "Leanne",
		lastname: "Graham",
		email: "sincere@april.biz",
		password: "db803fa8-d08f-434c-b25a-4e5e71c8c97d",
		access_token: null,
	};

	it("User registration", (done) => {
		apolloClient
			.mutate({
				mutation: gql`
					mutation (
						$firstname: String!
						$lastname: String!
						$email: String!
						$password: String!
					) {
						register(
							user: {
								firstname: $firstname
								lastname: $lastname
								email: $email
								password: $password
							}
						) {
							message
							error
						}
					}
				`,
				variables: { ...testUser },
			})
			.then(({ data }) => {
				const result = data.register;

				expect(result).toHaveProperty("message");
				expect([
					"Votre compte à été crée. Vous pouvez vous connectez",
					"Cet utilisateur existe déjà",
				]).toContain(result.message);
				done();
			});
	});

	it("User login", (done) => {
		apolloClient
			.query({
				query: gql`
					query ($email: String!, $password: String!) {
						login(user: { email: $email, password: $password }) {
							... on MsgType {
								message
								error
							}
							... on UserType {
								email
								firstname
								lastname
								access_token
								connected
							}
						}
					}
				`,
				variables: { ...testUser },
			})
			.then(({ data }) => {
				const result = data.login;

				if (result.__typename === "MsgType" && result.error) {
					const error = result.user;
					expect(error).toHaveProperty("message");
					expect([
						"Vos identifiants de connexion sont incorrectes",
						"Cet utilisateur n'existe pas",
					]).toContain(error.message);
					done();
					return;
				}

				const user = result;
				testUser = user;

				expect(user).toHaveProperty("email", testUser.email);
				expect(user).toHaveProperty("firstname", testUser.firstname);
				expect(user).toHaveProperty("lastname", testUser.lastname);
				expect(user).toHaveProperty("connected", true);
				expect(testUser.access_token).not.toBeNull();
				done();
			});
	});

	it("Get user with session token (me or viewer)", (done) => {
		apolloClient
			.query({
				query: gql`
					query ($access_token: String!) {
						me(access_token: $access_token) {
							... on MsgType {
								message
								error
							}
							... on UserType {
								email
								firstname
								lastname
								access_token
								connected
							}
						}
					}
				`,
				variables: { ...testUser },
			})
			.then(({ data }) => {
				if (data.me.__typename === "MsgType" && data.me.error) {
					const error = data.me.user;
					expect(data.logout.error).toBe(true);
					expect(error).toHaveProperty("message");
					expect(error.message).toEqual("Cet utilisateur n'existe pas");
					done();
					return;
				}

				const user = data.me;
				testUser = user;

				expect(user).toHaveProperty("email", testUser.email);
				expect(user).toHaveProperty("firstname", testUser.firstname);
				expect(user).toHaveProperty("lastname", testUser.lastname);
				expect(user).toHaveProperty("connected", true);
				expect(testUser.access_token).not.toBeNull();
				done();
			});
	});

	it("User logout", (done) => {
		apolloClient
			.query({
				query: gql`
					query ($access_token: String!) {
						logout(access_token: $access_token) {
							... on MsgType {
								message
								error
							}
						}
					}
				`,
				variables: { ...testUser },
			})
			.then(({ data }) => {
				const result = data.logout;

				expect(result.error).toBe(false);
				expect(result.message).toEqual("Vous êtes déconnecté");
				done();
			});
	});

	it("Delete user at the end", (done) => {
		apolloClient
			.mutate({
				mutation: gql`
					mutation ($access_token: String!) {
						deleteUser(access_token: $access_token) {
							... on MsgType {
								message
								error
							}
						}
					}
				`,
				variables: { ...testUser },
			})
			.then(({ data }) => {
				const result = data.deleteUser;

				if (result.__typename === "MsgType" && result.error) {
					expect(result.error).toBe(true);
					expect(result).toHaveProperty("message");
					expect(result.message).toEqual("Cet utilisateur n'existe pas");
					done();
				} else {
					testUser = { ...testUser, access_token: null, connected: false };
					expect(result.message).toEqual("L'utilsateur à été supprimé");
					done();
				}
			});
	});
});
